<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::post('/search', 'SearchController@index');

Route::prefix('/note')->group(function () {
    Route::get('/{id}', 'NotesController@index')->where(['id' => '[0-9]+']);
    Route::get('/create', 'NotesController@create');
    Route::post('/store', 'NotesController@store');
    Route::get('/{id}/edit', 'NotesController@edit')->where(['id' => '[0-9]+']);
    Route::post('/{id}/update', 'NotesController@update')->where(['id' => '[0-9]+']);
    Route::get('/{id}/delete', 'NotesController@destroy')->where(['id' => '[0-9]+']);
});


Route::post('/public_key/{id}', 'AjaxController@index');
Route::get('/public/{name}', 'NotesController@show');