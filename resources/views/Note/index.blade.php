@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="note-item">
            <a href="#" class="a-link">
                <h1 class="list-group-item" style="background: {{ $note->getColors->color }}">{{ $note->title }}</h1>
            </a>
            <div class="d-flex justify-content-between">
                <p class="m-0 p-0">Create date: <span class="badge badge-primary">{{ $note->created_at }}</span></p>
                <p class="m-0 p-0">Delete date: <span class="badge badge-primary">{{ $note->getDateDelete('d-m-y H:i:s') }}</span></p>
            </div>
            @if($note->getTegs->first())
                <p class="m-0 p-0">Tegs:
                @foreach($note->getTegs as $teg)
                    <span class="badge badge-primary">{{ $teg->teg }}</span>
                @endforeach
                </p>
            @endif
            <p class="list-group-item note-text" >{{ $note->description }}</p>

            @if($note->getFiles && file_exists(public_path().'/'.$note->getFiles->path))
                <a href="{{ public_path().'/'.$note->getFiles->path }}" download class="btn btn-primary">Download</a>
            @else
                <span>No file</span>
            @endif
        </div>
    </div>
@endsection