@extends('layouts.app')

@section('content')
    <div class="container">
        <form action="{{ url('/note/'.$note->id.'/update') }}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="input-group mb-3 input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text">Title: </span>
                </div>
                <input type="text" class="form-control" name="title" value="{{ $note->title }}">
            </div>

            <div class="form-group">
                <label for="description">Description:</label>
                <textarea class="form-control" rows="5" id="description" name="description">{{ $note->description }}</textarea>
            </div>

            <div class="input-group mb-3 input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text">Storage date: </span>
                </div>
                <select class="form-control" id="storage" name="storage">
                    <option @if($note->getDate->date == 1) selected @endif value="1">1</option>
                    <option @if($note->getDate->date == 15) selected @endif value="15">15</option>
                    <option @if($note->getDate->date == 30) selected @endif value="30">30</option>
                </select>
            </div>

            <div class="input-group mb-3 input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text">Color: </span>
                </div>
                <select class="form-control" id="storage" name="color">
                    <option @if($note->getColors->color == 'purple') selected @endif value="purple">purple</option>
                    <option @if($note->getColors->color == 'black') selected @endif value="black">black</option>
                    <option @if($note->getColors->color == 'orange') selected @endif value="orange">orange</option>
                    <option @if($note->getColors->color == 'grey') selected @endif value="grey">grey</option>
                </select>
            </div>

            <div class="input-group mb-3 input-group" id="div-teg">
                <div class="input-group-prepend" id="newTeg">
                    <span class="input-group-text btn btn-primary">Teg: </span>
                </div>
                @if($note->getTegs->count())
                    @foreach($note->getTegs as $teg)
                        <input type="text" class="form-control" name="teg[]" value="{{ $teg->teg }}">
                    @endforeach
                @else
                    <input type="text" class="form-control" name="teg[]">
                @endif
            </div>

            <div class="custom-file fl_upld">
                <input type="file" class="custom-file-input" id="fl_inp" name="file">
                <label id="fl_nm" class="custom-file-label" for="customFile">
                    @if($note->getFiles)
                    {{ $note->getFiles->path }}
                    @else
                    No file
                    @endif
                </label>
            </div>

            <div>
                <button type="submit" class="btn btn-primary mt-2">Submit</button>
            </div>
        </form>
    </div>
@endsection