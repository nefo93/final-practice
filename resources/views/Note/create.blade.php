@extends('layouts.app')

@section('content')
    <div class="container">
        <form action="{{ url('/note/store') }}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="input-group mb-3 input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text">Title: </span>
                </div>
                <input type="text" class="form-control" name="title" value="{{ old('title') }}">
            </div>

            <div class="form-group">
                <label for="description">Description:</label>
                <textarea class="form-control" rows="5" id="description" name="description">{{ old('description') }}</textarea>
            </div>

            <div class="input-group mb-3 input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text">Storage date: </span>
                </div>
                <select class="form-control" id="storage" name="storage">
                    <option selected value="1">1</option>
                    <option value="15">15</option>
                    <option value="30">30</option>
                </select>
            </div>

            <div class="input-group mb-3 input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text">Color: </span>
                </div>
                <select class="form-control" id="storage" name="color">
                    <option selected value="purple">purple</option>
                    <option value="black">black</option>
                    <option value="orange">orange</option>
                    <option value="grey">grey</option>
                </select>
            </div>

            <div class="input-group mb-3 input-group" id="div-teg">
                <div class="input-group-prepend" id="newTeg">
                    <span class="input-group-text btn btn-primary">Teg: </span>
                </div>
                <input type="text" class="form-control" name="teg[]">
            </div>

            <input type="file" name="file" value="{{ old('file') }}">
            <div>
                <button type="submit" class="btn btn-primary mt-2">Submit</button>
            </div>
        </form>
    </div>
@endsection