@extends('layouts.app')

@section('content')
<div>
    <h1 class="text-center">Home</h1>
    <table class="table table-dark text-center">
        <thead>
        <tr>
            <th>Title</th>
            <th>Description</th>
            <th>Tegs</th>
            <th>Create</th>
            <th>Delete</th>
            <th>Public_url</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($notes->reverse() as $note)
        <tr style="background: {{ $note->getColors->color }}" class="body-table">
            <td>{{ $note->title }}</td>
            <td>{{ mb_strimwidth($note->description, 0, 20) }} ...</td>
            <td>
                @if($note->getTegs->count())
                    @foreach($note->getTegs as $teg)
                        <span class="badge badge-dark">{{ $teg->teg }}</span>
                    @endforeach
                @endif
            </td>
            <td>{{ $note->created_at->format('d-m-Y') }}</td>
            <td>{{ $note->getDateDelete('d-m-y') }}</td>
            <td class="ajaxPublic">
                @if($note->public_key)
                    <span class="text-white">{{ url('/public').'/'.$note->public_key }}</span>
                @else
                    <span class="btn btn-primary inputPublic" data-content="{{ $note->id }}">Get</span>
                    <span class="text-white publicUrl"></span>
                @endif
            </td>
            <td>
                <div>
                    <a href="{{ url('/note/'.$note->id) }}" class="btn btn-info">Show</a>
                    <a href="{{ url('/note/'.$note->id.'/edit') }}" class="btn btn-primary">Edit</a>
                    <a href="{{ url('/note/'.$note->id.'/delete') }}" class="btn btn-danger">Delete</a>
                </div>
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>
</div>
@endsection
