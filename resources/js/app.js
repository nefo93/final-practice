
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

Vue.component('example-component', require('./components/ExampleComponent.vue'));

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key)))

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app'
});


$('#newTeg').on('click', function () {
    $('#div-teg').append('<input type="text" class="form-control" name="teg[]">');
});

$("#fl_inp").change(function(){
    var filename = $(this).val().replace(/.*\\/, "");
    $("#fl_nm").html(filename);
});

$('.inputPublic').click(function () {
   $(this).hide();
   var div = $(this);
   var id = div.data('content');
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: '/public_key/' + id,
        dataType : 'json',
        type: 'POST',
        success:function(data) {
            div.siblings('.publicUrl').text(data);
        }
    });
});