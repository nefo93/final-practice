<?php

namespace App\Services\Ajax;

use App\Interfaces\Services\Ajax\AjaxInterface;
use App\Model\Note;

class AjaxService implements AjaxInterface
{
    /**
     * @var Note
     */
    private $note;

    /**
     * AjaxService constructor.
     * @param Note $note
     */
    public function __construct(Note $note)
    {
        $this->note = $note;
    }

    /**
     * The function accepts the current record ID. Generates a random key, writes to the database and returns
     *
     * @param int $id
     * @return string
     */
    public function getPublicKey(int $id):string
    {
        $note = $this->note->getNote(['id' => $id])->first();
        $public_key = str_random(10);

        $note->public_key = $public_key;
        $note->save();

        return $public_key;
    }
}