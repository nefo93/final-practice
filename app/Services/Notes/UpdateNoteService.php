<?php

namespace App\Services\Notes;

use App\Http\Requests\StoreNoteRequest;
use App\Interfaces\Services\Notes\UpdateNoteInterface;
use App\Model\NoteFile;
use App\Model\NoteTeg;

class UpdateNoteService implements UpdateNoteInterface
{
    /**
     * @var NoteTeg
     */
    protected $noteTeg;

    /**
     * @var NoteFile
     */
    protected $noteFile;

    /**
     * UpdateNoteService constructor.
     * @param NoteTeg $teg
     * @param NoteFile $noteFile
     */
    public function __construct(NoteTeg $teg, NoteFile $noteFile)
    {
        $this->noteTeg = $teg;
        $this->noteFile = $noteFile;
    }

    /**
     * The function takes the data and model. And updates the tables.
     *
     * @param StoreNoteRequest $request
     * @param $note
     */
    public function saveAll(StoreNoteRequest $request, $note)
    {
        $insertNote = $this->getArrayNote($request);
        $insertDate = $this->getArrayNoteDate($request);
        $insertColor = $this->getArrayNoteColor($request);

        //update table 'notes'
        $this->update($insertNote, $note);

        //update table 'note_dates'
        $this->update($insertDate, $note->getDate);

        //update table 'note_colors'
        $this->update($insertColor, $note->getColors);

        //delete all tegs and create new tegs
        $this->updateTegs($request->get('teg'), $note->getTegs, $note->id);

        //if the file is. We delete the old file and save the new one.
        if($request->file('file')) {
            $this->updateFile($request->file('file'), $note);
        }

    }

    /**
     * Accepts data array and model. Updates the passed model.
     *
     * @param array $insert
     * @param $model
     */
    protected function update(array $insert, $model)
    {
        $model->update($insert);
    }

    /**
     * The function takes a request from it receives the title and description and returns as an array.
     *
     * @param StoreNoteRequest $request
     * @return array
     */
    protected function getArrayNote(StoreNoteRequest $request):array
    {
        return [
            'title' => $request->get('title'),
            'description' => $request->get('description')
        ];
    }

    /**
     * The function takes a request from it receives the date and returns as an array.
     *
     * @param StoreNoteRequest $request
     * @return array
     */
    protected function getArrayNoteDate(StoreNoteRequest $request):array
    {
        return [
            'date' => $request->get('storage')
        ];
    }

    /**
     * The function takes a request from it receives the color and returns as an array.
     *
     * @param StoreNoteRequest $request
     * @return array
     */
    protected function getArrayNoteColor(StoreNoteRequest $request):array
    {
        return [
            'color' => $request->get('color')
        ];
    }

    /**
     * The function accepts an array of new tags, a collection of old tags and ID of the current record.
     * Then remove the old tags from the collection and write new ones.
     *
     * @param array $tegs
     * @param $collection
     * @param int $noteId
     */
    protected function updateTegs(array $tegs, $collection, int $noteId)
    {
        $this->deleteTegs($collection);

        foreach ($tegs as $teg) {
            if($teg) {
                $this->noteTeg->create(['note_id' => $noteId, 'teg' => $teg]);
            }
        }
    }

    /**
     * The function takes a collection and deletes everything inside it.
     *
     * @param $collection
     */
    protected function deleteTegs($collection)
    {
        foreach ($collection as $model) {
            $model->delete();
        }
    }

    /**
     * The function accepts a new file and model.
     * Creates a new file and a new name.
     * If there is an entry in the model, deletes the old file and updates the model.
     * If there is no entry, add the entry.
     *
     * @param $file
     * @param $note
     * @return bool
     */
    protected function updateFile($file, $note)
    {
        $newPathFile = $this->createFile($file);

        if($note->getFiles) {
            $this->deleteFile($note->getFiles->path);
            $this->update(['path' => $newPathFile], $note->getFiles);

            return true;
        }

        $this->noteFile->create(['note_id' => $note->id, 'path' => $newPathFile]);

        return false;
    }

    /**
     * Accepts the path to the file. Checks if a file exists, if it exists, deletes it.
     *
     * @param string $path
     */
    public function deleteFile(string $path)
    {
        $path = public_path().'/'.$path;

        if (file_exists($path)) {
            unlink($path);
        }

    }

    /**
     * Accepts the file and transmutes. returns the path to the file.
     *
     * @param $file
     * @return string
     */
    protected function createFile($file):string
    {
        $fileName = $file->getFileName().$file->getSize().'.'.$file->getClientOriginalExtension();
        $newFilePath = 'images/notes';
        $file->move(public_path().'/'.$newFilePath, $fileName);

        return $newFilePath.'/'.$fileName;
    }
}