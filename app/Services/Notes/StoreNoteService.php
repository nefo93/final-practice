<?php

namespace App\Services\Notes;

use App\Http\Requests\StoreNoteRequest;
use App\Interfaces\Services\Notes\StoreNoteInterface;
use App\Model\Note;
use App\Model\NoteColor;
use App\Model\NoteDate;
use App\Model\NoteFile;
use App\Model\NoteTeg;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

/**
 * Class StoreNoteService
 * @package App\Services\Notes
 */
class StoreNoteService implements StoreNoteInterface
{
    /**
     * @var StoreNoteRequest
     */
    protected $request;

    /**
     * @var Note
     */
    protected $note;

    /**
     * @var NoteDate
     */
    protected $noteDate;

    /**
     * @var NoteColor
     */
    protected $noteColor;

    /**
     * @var NoteFile
     */
    protected $noteFile;

    /**
     * @var NoteTeg
     */
    protected $noteTeg;

    /**
     * StoreNoteService constructor.
     * @param Note $note
     * @param NoteDate $date
     * @param NoteColor $color
     * @param NoteFile $file
     * @param NoteTeg $teg
     */
    public function __construct(
        Note $note,
        NoteDate $date,
        NoteColor $color,
        NoteFile $file,
        NoteTeg $teg
    )
    {
        $this->note = $note;
        $this->noteDate = $date;
        $this->noteColor = $color;
        $this->noteFile = $file;
        $this->noteTeg = $teg;
    }

    /**
     * @param StoreNoteRequest $request
     */
    public function setRequest(StoreNoteRequest $request)
    {
        $this->request = $request;
    }

    /**
     * The function saves all the data in the database.
     */
    public function saveAll()
    {
        //save in table 'notes' and return id
        $noteId = $this->saveNoteAndGetId();

        //save in table 'note_colors'
        $this->saveInsert($this->noteColor, ['color' => $this->request->get('color'), 'note_id' => $noteId]);

        //save in table 'note_dates'
        $this->saveInsert($this->noteDate, ['date' => $this->request->get('storage'), 'note_id' => $noteId]);

        //save in table 'note_tegs'
        $this->saveTegs($this->noteTeg, $this->request->get('teg'), $noteId);

        if($this->request->file('file')) {
            $pathFile = $this->getFilepathAndSave();
            $this->saveInsert($this->noteFile, ['path' => $pathFile, 'note_id' => $noteId]);
        }
    }

    /**
     * The function saves the new record and returns the ID record.
     *
     * @return int
     */
    protected function saveNoteAndGetId():int
    {
        $insert = [
            'user_id' => Auth::id(),
            'title' => $this->request->get('title'),
            'description' => $this->request->get('description')
        ];

        return $this->note->create($insert)->id;
    }

    /**
     * The function accepts a model and an array of stored data.
     *
     * @param Model $model
     * @param array $insert
     */
    protected function saveInsert(Model $model, array $insert)
    {
        $model->create($insert);
    }

    /**
     * The function saves the file and returns the new path to the file.
     *
     * @return string
     */
    protected function getFilePathAndSave():string
    {
        $file = $this->request->file('file');
        $fileName = $file->getFileName().$file->getSize().'.'.$file->getClientOriginalExtension();
        $newFilePath = 'images/notes';
        $file->move(public_path().'/'.$newFilePath, $fileName);

        return $newFilePath.'/'.$fileName;
    }

    /**
     * The function accepts a tag model, an array of tags and the ID of the current record.
     *
     * @param Model $model
     * @param array $tegs
     * @param $noteId
     */
    protected function saveTegs(Model $model, array $tegs, $noteId)
    {
        foreach($tegs as $teg) {
            if($teg) {
                $model->create(['note_id' => $noteId, 'teg' => $teg]);
            }
        }
    }
}