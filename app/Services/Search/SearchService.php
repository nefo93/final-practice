<?php

namespace App\Services\Search;

use App\Http\Requests\SearchRequest;
use App\Interfaces\Services\Search\SearchInterface;
use App\Model\Note;

class SearchService implements SearchInterface
{
    /**
     * @var Note
     */
    private $noteModel;

    /**
     * SearchService constructor.
     * @param Note $note
     */
    public function __construct(Note $note)
    {
        $this->noteModel = $note;
    }

    /**
     * Accepts search data and returns a collection of records.
     *
     * @param SearchRequest $request
     * @return mixed
     */
    public function getNotes(SearchRequest $request)
    {
        $notes = $this->noteModel->getNotesBySearch($request->get('type'), $request->get('content'));

        return $notes;
    }

}