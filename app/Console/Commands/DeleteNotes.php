<?php

namespace App\Console\Commands;

use App\Model\Note;
use Illuminate\Console\Command;

class DeleteNotes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:DeleteNotes';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete notes';

    /**
     * @var Note
     */
    protected $notes;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Note $model)
    {
        parent::__construct();
        $this->notes = $model;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $notes = $this->notes->with('getDate')->get();

        foreach ($notes as $note) {
            if($note->getDateDelete() == date('d-m-y')) {
                $note->delete();
            }
        }
    }
}
