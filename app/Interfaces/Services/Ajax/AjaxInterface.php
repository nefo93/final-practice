<?php

namespace App\Interfaces\Services\Ajax;

interface AjaxInterface
{
    public function getPublicKey(int $id):string ;
}