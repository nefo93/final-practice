<?php

namespace App\Interfaces\Services\Search;

use App\Http\Requests\SearchRequest;

interface SearchInterface
{
    public function getNotes(SearchRequest $request);
}