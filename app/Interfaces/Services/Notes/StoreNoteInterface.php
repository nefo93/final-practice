<?php

namespace App\Interfaces\Services\Notes;

use App\Http\Requests\StoreNoteRequest;

interface StoreNoteInterface
{
    public function setRequest(StoreNoteRequest $request);

    public function saveAll();
}