<?php

namespace App\Interfaces\Services\Notes;

use App\Http\Requests\StoreNoteRequest;

interface UpdateNoteInterface
{
    public function saveAll(StoreNoteRequest $request, $note);
}