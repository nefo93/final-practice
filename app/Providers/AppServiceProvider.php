<?php

namespace App\Providers;

use App\Interfaces\Services\Ajax\AjaxInterface;
use App\Interfaces\Services\Notes\StoreNoteInterface;
use App\Interfaces\Services\Notes\UpdateNoteInterface;
use App\Interfaces\Services\Search\SearchInterface;
use App\Services\Ajax\AjaxService;
use App\Services\Notes\StoreNoteService;
use App\Services\Notes\UpdateNoteService;
use App\Services\Search\SearchService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(StoreNoteInterface::class, StoreNoteService::class);
        $this->app->bind(UpdateNoteInterface::class, UpdateNoteService::class);
        $this->app->bind(AjaxInterface::class, AjaxService::class);
        $this->app->bind(SearchInterface::class, SearchService::class);
    }
}
