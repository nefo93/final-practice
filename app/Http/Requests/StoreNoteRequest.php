<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreNoteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'storage' => 'integer',
            'title' => 'required|min:4|string',
            'description' => 'required|min:20|string',
            'teg.*' => 'nullable|string',
            'color' => 'string|alpha',
            'file' => 'file|max:50000|mimes:jpg,jpeg,png,doc,docx,txt'
        ];
    }

    public function messages()
    {
        return parent::messages();
    }
}
