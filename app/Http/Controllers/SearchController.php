<?php

namespace App\Http\Controllers;

use App\Http\Requests\SearchRequest;
use App\Interfaces\Services\Search\SearchInterface;

class SearchController extends Controller
{

    /**
     * @var SearchInterface
     */
    private $searchService;

    /**
     * SearchController constructor.
     * @param SearchInterface $search
     */
    public function __construct(SearchInterface $search)
    {
        $this->searchService = $search;
    }

    /**
     * @param SearchRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(SearchRequest $request)
    {
        $notes = $this->searchService->getNotes($request);

        return view('home', ['notes' => $notes]);
    }
}
