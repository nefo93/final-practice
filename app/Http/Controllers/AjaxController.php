<?php

namespace App\Http\Controllers;

use App\Interfaces\Services\Ajax\AjaxInterface;

class AjaxController extends Controller
{
    private $ajaxService;

    /**
     * AjaxController constructor.
     * @param AjaxInterface $ajax
     */
    public function __construct(AjaxInterface $ajax)
    {
        $this->ajaxService = $ajax;
    }

    public function index(int $id)
    {
        $url = $this->ajaxService->getPublicKey($id);
        $url = url('/public').'/'.$url;
        return response([$url]);
    }
}
