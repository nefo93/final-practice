<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreNoteRequest;
use App\Interfaces\Services\Notes\StoreNoteInterface;
use App\Interfaces\Services\Notes\UpdateNoteInterface;
use App\Model\Note;

class NotesController extends Controller
{
    /**
     * @var Note
     */
    protected $notes;

    /**
     * @var StoreNoteInterface
     */
    protected $storeNote;

    /**
     * @var UpdateNoteInterface
     */
    protected $updateNote;

    /**
     * NotesController constructor.
     * @param Note $model
     * @param StoreNoteInterface $storeNote
     * @param UpdateNoteInterface $updateNote
     */
    public function __construct(
        Note $model,
        StoreNoteInterface $storeNote,
        UpdateNoteInterface $updateNote
    )
    {
        $this->notes = $model;
        $this->storeNote = $storeNote;
        $this->updateNote = $updateNote;
    }

    /**
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function index(int $id)
    {
        $note = $this->notes->getNote(['id' => $id])->first();

        if($note) {
            return view('Note.index', ['note' => $note]);
        }

        return redirect()->route('home');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('Note.create');
    }

    /**
     * @param StoreNoteRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreNoteRequest $request)
    {
        $this->storeNote->setRequest($request);
        $this->storeNote->saveAll();

        return redirect()->route('home');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function edit($id)
    {
        $note = $this->notes->getNote(['id' => $id])->first();

        if($note) {
            return view('Note.update', ['note' => $note]);
        }

        return redirect()->route('home');
    }

    /**
     * @param StoreNoteRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(StoreNoteRequest $request, $id)
    {
        $note = $this->notes->getNote(['id' => $id])->first();

        $this->updateNote->saveAll($request, $note);

        return redirect()->route('home');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $this->notes->getNote(['id' => $id])->delete();

        return redirect()->route('home');
    }

    /**
     * @param $public_key
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($public_key)
    {
        $note = $this->notes->getNoteByPublicKey($public_key);

        return view('Note.index', ['note' => $note]);
    }

}
