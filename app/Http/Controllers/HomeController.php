<?php

namespace App\Http\Controllers;

use App\Model\Note;

class HomeController extends Controller
{
    /**
     * @var Note $model
     */
    protected $notes;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Note $model)
    {
        $this->middleware('auth');
        $this->notes = $model;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $notes = $this->notes->getNote()->get();

        return view('home', ['notes' => $notes]);
    }

}
