<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class NoteDate extends Model
{
    protected $table = 'note_dates';

    protected $fillable = [
        'note_id',
        'date'
    ];
}
