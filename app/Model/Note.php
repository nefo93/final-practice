<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Note extends Model
{
    protected $table = 'notes';

    protected $fillable = [
        'user_id',
        'title',
        'description'
    ];

    public function getTegs()
    {
        return $this->hasMany(NoteTeg::class);
    }

    public function getFiles()
    {
        return $this->hasOne(NoteFile::class);
    }

    public function getDate()
    {
        return $this->hasOne(NoteDate::class);
    }

    public function getColors()
    {
        return $this->hasOne(NoteColor::class);
    }

    /**
     * @param array $array
     * @return mixed
     */
    public function getNote(array $array = [])
    {
        $array['user_id'] = Auth::id();

        return $this
            ->where($array)
            ->with(['getDate', 'getTegs', 'getFiles', 'getColors']);
    }

    /**
     * The function returns the date when the record is deleted.
     * As a parameter, it can take the format of the return date.
     *
     * @param string $format
     * @return false|string
     */
    public function getDateDelete(string $format = 'd-m-y')
    {
        $dateCreated = strtotime($this->created_at);
        $dateDelete = date($format,($dateCreated + 86400 * $this->getDate->date));

        return $dateDelete;
    }

    /**
     * The function accepts a public key string and returns the entry itself.
     *
     * @param string $public
     * @return mixed
     */
    public function getNoteByPublicKey(string $public)
    {
        return $this->where('public_key', $public )->first();
    }

    /**
     * The function returns the collection of records according to their parameters.
     *
     * @param string $key
     * @param string $value
     * @return mixed
     */
    public function getNotesBySearch(string $key, string $value)
    {
        return $this->where($key, 'LIKE', '%'.$value.'%')->where('user_id', Auth::id())->get();
    }
}
