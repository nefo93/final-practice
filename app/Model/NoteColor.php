<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class NoteColor extends Model
{
    protected $table = 'note_colors';

    protected $fillable = [
        'note_id',
        'color'
    ];
}
