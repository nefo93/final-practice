<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class NoteFile extends Model
{
    protected $table = 'note_files';

    protected $fillable = [
        'note_id',
        'path'
    ];
}
