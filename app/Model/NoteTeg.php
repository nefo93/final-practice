<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class NoteTeg extends Model
{
    protected $table = 'note_tegs';

    protected $fillable = [
        'note_id',
        'teg'
    ];
}
