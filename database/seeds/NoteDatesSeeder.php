<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class NoteDatesSeeder extends Seeder
{
    const COUNT = 30;

    /**
     * Run the database seeds.
     *
     * @return void
     * @throws Exception
     */
    public function run()
    {
        for ($i = 1; $i <= self::COUNT; $i++) {
            $insert[] = [
                'note_id' => $i,
                'date' => '1',
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ];
        }

        DB::table('note_dates')->insert($insert);
    }
}
