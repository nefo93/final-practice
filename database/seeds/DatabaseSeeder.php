<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersSeeder::class);
        $this->call(NotesSeeder::class);
        $this->call(NoteTegsSeeder::class);
        $this->call(NoteFilesSeeder::class);
        $this->call(NoteDatesSeeder::class);
        $this->call(NoteColorsSeeder::class);
    }
}
