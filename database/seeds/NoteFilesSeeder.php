<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class NoteFilesSeeder extends Seeder
{
    const COUNT = 30;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= self::COUNT; $i++) {
            $insert[] = [
                'note_id' => $i,
                'path' => 'images/notes/1.jpg',
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ];
        }

        DB::table('note_files')->insert($insert);
    }
}
