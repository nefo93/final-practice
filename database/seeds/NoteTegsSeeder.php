<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class NoteTegsSeeder extends Seeder
{
    const COUNT = 30;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= self::COUNT; $i++) {
            $insert[] = [
                'note_id' => rand(1, 10),
                'teg' => str_random(5),
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ];
        }

        DB::table('note_tegs')->insert($insert);
    }
}
