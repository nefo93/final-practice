<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class NoteColorsSeeder extends Seeder
{
    const COUNT = 30;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $color = ['purple','black', 'orange', 'grey'];
        for ($i = 1; $i <= self::COUNT; $i++) {
            $insert[] = [
                'note_id' => $i,
                'color' => $color[rand(0, 3)],
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ];
        }

        DB::table('note_colors')->insert($insert);
    }
}
