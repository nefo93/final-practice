<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersSeeder extends Seeder
{
    const COUNT = 5;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= self::COUNT; $i++) {
            $insert[] = [
                'name' => 'test-'.$i,
                'email' => 'test'.$i.'@mail.com',
                'password' => bcrypt('031293'),
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ];
        }

        DB::table('users')->insert($insert);
    }
}
