<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class NotesSeeder extends Seeder
{
    const COUNT = 30;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= self::COUNT; $i++) {
            $insert[] = [
                'title' => 'title-'.$i,
                'description' => str_random(300),
                'user_id' => rand(1, 5),
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ];
        }

        DB::table('notes')->insert($insert);
    }
}
